﻿
using System.Collections.Generic;

namespace SortAlgorithms.Implementations
{
	public static class QuickSort
	{
		public static void QuickSortMergeWithMethod<T>(List<T> orderedList, Comparer<T> comparer, int leftIndex = 0)
		{
			comparer = comparer ?? Comparer<T>.Default;
			orderedList.RecursiveQuickSort(comparer, leftIndex, orderedList.Count - 1);
		}

		private static void RecursiveQuickSort<T>(this List<T> orderedList, Comparer<T> comparer, int leftIndex, int rightIndex)
		{
			List<T> newList = new List<T>();
			newList.AddRange(orderedList);

			var pivot = PartionSort(newList, comparer, leftIndex, rightIndex);

			if (pivot > 1)
				RecursiveQuickSort<T>(newList, comparer, leftIndex, pivot - 1);
			if (pivot + 1 < rightIndex)
				RecursiveQuickSort<T>(newList, comparer, leftIndex, pivot + 1);
		}

		private static int PartionSort<T>(this List<T> orderedList, Comparer<T> comparer, int leftIndex, int rightIndex)
		{
			int pivot = 0;
			if ((rightIndex - leftIndex) % 2 == 0)
			{
				pivot = (rightIndex - leftIndex) / 2;
			}
			else
			{
				pivot = (rightIndex - leftIndex) / 2 + 1;
			}
			for (int i = leftIndex; i <= (rightIndex - 1); i++)
			{
				// check if collection[i] <= pivotValue
				if (comparer.Compare(orderedList[i], orderedList[pivot]) > 0)
				{
					//var temp = orderedList[i];
					//orderedList[i] = orderedList[pivot];
					//orderedList[pivot] = temp;
					orderedList.Swap(pivot,i);
					pivot++;
				}
			}
			return pivot;
		}

	}
}
