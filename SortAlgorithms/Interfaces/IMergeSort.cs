﻿

using System.Collections.Generic;

namespace SortAlgorithms.Interfaces
{
	public interface IMergeSort
	{

		List<T> MergeSortRecursiveMethod<T>(List<T> ordereredList, Comparer<T> comparer, int leftIndex, int rightIndex);

	    List<T> MergeList<T>(List<T> beforeList, List<T> afterList, Comparer<T> comparer);
	}
}
