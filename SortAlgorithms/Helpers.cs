﻿
using System.Collections.Generic;

namespace SortAlgorithms
{
	public  static  class Helpers
	{
		public static void Swap<T>(this List<T> orderedList, int leftItem, int rightItem)
		{
			var temp = orderedList[leftItem];
			orderedList[leftItem] = orderedList[rightItem];
			orderedList[rightItem] = temp;
		}
	}
}
