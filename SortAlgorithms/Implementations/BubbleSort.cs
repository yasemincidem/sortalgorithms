﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortAlgorithms.Implementations
{
	public static class BubbleSort
	{
		public static async Task<Dictionary<string, List<T>>> BubleSortWithMethod<T>(List<T> orderedList, Comparer<T> comparer)
		{
			comparer = comparer ?? Comparer<T>.Default;

			List<T> ascendingList = await Task.Run(() => orderedList.AscendingBubleSort(comparer));

			List<T> descendingList = await Task.Run(() => ascendingList.DescendingBubleSort(comparer));

			Dictionary<string, List<T>> bubleSort = new Dictionary<string, List<T>>()
			{
				{"ascendingList",ascendingList },
				{"descendingList", descendingList}
			};

			return bubleSort;
		}

		public static List<T> AscendingBubleSort<T>(this List<T> ascList, Comparer<T> comparer)
		{
			List<T> newList = new List<T>();
			newList.AddRange(ascList);
			for (int index = 0; index < newList.Count - 1; index++)
			{
				if (comparer.Compare(newList[index], newList[index + 1]) > 0)
				{
					//var temp = newList[index];
					//newList[index] = newList[index + 1];
					//newList[index + 1] = temp;
					ascList.Swap(index+1,index);
				}
			}
			return newList;
		}

		public static List<T> DescendingBubleSort<T>(this List<T> descList, Comparer<T> comparer)
		{
			List<T> newList = new List<T>();
			newList.AddRange(descList);
			newList.Reverse();
			return newList;
		}
	}
}
