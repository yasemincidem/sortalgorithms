﻿using System;
using System.Collections.Generic;
using System.Linq;
using static System.Console;

namespace SortAlgorithms.Implementations
{
    public class ReviewInformation
    {
        public string Name { get; set; }
        public double Rating { get; set; }
    }
    public class RecommendationSystem
    {
        public Dictionary<string, List<ReviewInformation>> movieDictionary = new Dictionary<string, List<ReviewInformation>>();
        public Dictionary<string, List<ReviewInformation>> AddProductReview()
        {

            List<ReviewInformation> reviews = new List<ReviewInformation>();

            reviews.Add(new ReviewInformation() { Name = "Wile E Coyote", Rating = 4.5 });
            reviews.Add(new ReviewInformation() { Name = "Bugs Bunny", Rating = 2.5 });
            reviews.Add(new ReviewInformation() { Name = "Elmer Fudd", Rating = 5.0 });
            reviews.Add(new ReviewInformation() { Name = "Foghorn Leghorn", Rating = 2.0 });
            movieDictionary.Add("ACME Industrial Rocket Pack", reviews);

            List<ReviewInformation> reviews1 = new List<ReviewInformation>();

            reviews1.Add(new ReviewInformation() { Name = "Wile E Coyote", Rating = 5.0 });
            reviews1.Add(new ReviewInformation() { Name = "Bugs Bunny", Rating = 3.5 });
            reviews1.Add(new ReviewInformation() { Name = "Elmer Fudd", Rating = 1.0 });
            reviews1.Add(new ReviewInformation() { Name = "Foghorn Leghorn", Rating = 3.5 });
            reviews1.Add(new ReviewInformation() { Name = "Daffy Duck", Rating = 1.0 });
            movieDictionary.Add("ACME Super Sling Shot", reviews1);

            List<ReviewInformation> reviews2 = new List<ReviewInformation>();

            reviews2.Add(new ReviewInformation() { Name = "Wile E Coyote", Rating = 1.0 });
            reviews2.Add(new ReviewInformation() { Name = "Bugs Bunny", Rating = 3.5 });
            reviews2.Add(new ReviewInformation() { Name = "Elmer Fudd", Rating = 5.0 });
            reviews2.Add(new ReviewInformation() { Name = "Foghorn Leghorn", Rating = 4.0 });
            reviews2.Add(new ReviewInformation() { Name = "Daffy Duck", Rating = 4.0 });
            movieDictionary.Add("ACME X-Large Catapult", reviews2);

            List<ReviewInformation> review3 = new List<ReviewInformation>();

            review3.Add(new ReviewInformation() { Name = "Bugs Bunny", Rating = 3.5 });
            review3.Add(new ReviewInformation() { Name = "Elmer Fudd", Rating = 4.0 });
            review3.Add(new ReviewInformation() { Name = "Foghorn Leghorn", Rating = 5.0 });
            review3.Add(new ReviewInformation() { Name = "Daffy Duck", Rating = 2.5 });
            movieDictionary.Add("ACME Super Glue", review3);

            List<ReviewInformation> review4 = new List<ReviewInformation>();

            review4.Add(new ReviewInformation() { Name = "Wile E Coyote", Rating = 4.5 });
            review4.Add(new ReviewInformation() { Name = "Bugs Bunny", Rating = 5.0 });
            review4.Add(new ReviewInformation() { Name = "Foghorn Leghorn", Rating = 3.0 });
            movieDictionary.Add("ACME Jet Powered Roller Skates", review4);

            return movieDictionary;
        }

        public double CalculatePearsonCorellation(string product1, string product2)
        {
            List<ReviewInformation> sharedList = new List<ReviewInformation>();
            double sumProduct1Rating = 0.0f;
            double sumProduct2Rating = 0.0f;
            double sqrtProduct1Rating = 0.0f;
            double sqrtProduc2Rating = 0.0f;
            double criticSum = 0.0f;


            foreach (var item in movieDictionary[product1])
            {
                if (movieDictionary[product2].Count(r => r.Name == item.Name) != 0)
                {
                    sharedList.Add(item);
                }
            }
            foreach (var item in sharedList)
            {
                sumProduct1Rating +=
                    movieDictionary[product1].Where(r => r.Name == item.Name).Select(r => r.Rating).FirstOrDefault();

            }
            foreach (var item in sharedList)
            {
                sumProduct2Rating +=
                    movieDictionary[product2].Where(r => r.Name == item.Name).Select(r => r.Rating).FirstOrDefault();
            }
            foreach (var item in sharedList)
            {
                sqrtProduct1Rating +=
                    Math.Pow(
                        movieDictionary[product2].Where(r => r.Name == item.Name).Select(r => r.Rating).FirstOrDefault(),
                        2);
            }
            foreach (var item in sharedList)
            {
                sqrtProduc2Rating +=
                    Math.Pow(movieDictionary[product2].Where(r => r.Name == item.Name).Select(r => r.Rating).FirstOrDefault(), 2);
            }
            foreach (var item in sharedList)
            {
                criticSum +=
                    movieDictionary[product1].Where(r => r.Name == item.Name).Select(r => r.Rating).FirstOrDefault() *
                    movieDictionary[product2].Where(r => r.Name == item.Name).Select(r => r.Rating).FirstOrDefault();

            }

            //calculate pearson score
            double num = criticSum - (sumProduct1Rating * sumProduct2Rating / sharedList.Count);

            double density = (double)Math.Sqrt((sqrtProduct1Rating - Math.Pow(sumProduct1Rating, 2) / sharedList.Count)
                * ((sqrtProduc2Rating - Math.Pow(sumProduct2Rating, 2) / sharedList.Count)));

            if (density == 0)
            {
                return 0;
            }
            return num / density;
        }

        public void TopMatches(string product1)
        {
            Dictionary<string, List<ReviewInformation>> exxceptProduct1 = new Dictionary<string, List<ReviewInformation>>();
            movieDictionary = AddProductReview();
            double calculatedResult = 0.0f;

            List<ReviewInformation> calculateRating = new List<ReviewInformation>();

            foreach (var item in movieDictionary)
            {
                if (item.Key != product1)
                {
                    exxceptProduct1.Add(item.Key, item.Value);
                }

            }
            foreach (var item in exxceptProduct1)
            {
                calculatedResult = CalculatePearsonCorellation(product1, item.Key);
                calculateRating.Add(new ReviewInformation() { Name = item.Key, Rating = calculatedResult });
                WriteLine($@"ProductReview:{item.Key},CalculateResult={calculatedResult}");
            }
            var topMatchesResult = calculateRating[0];
            for (int i = 0; i <= calculateRating.Count - 1; i++)
            {
                if (calculateRating[i].Rating > 0)
                {
                    topMatchesResult = calculateRating[i];
                }
                else if (i != 3 && calculateRating[i].Rating > 0)
                {
                    if (calculateRating[i].Rating <= calculateRating[i + 1].Rating)
                    {
                        topMatchesResult = calculateRating[i];
                    }
                }
            }
            WriteLine($@"Top matches result product,{topMatchesResult.Name},{topMatchesResult.Rating}");
        }
    }
}
