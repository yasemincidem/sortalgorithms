﻿
using System.Collections.Generic;

namespace SortAlgorithms.Interfaces
{
	interface IInsertionSort
	{
		List<T> InsertionSortMethod<T>(List<T> orderedList, Comparer<T> comparer);
	}
}
