﻿using Nito.AsyncEx;
using System.Collections.Generic;
using System.Runtime.Remoting.Lifetime;
using static System.Console;

namespace SortAlgorithms
{
	class Program
	{
		public static  void Main(string[] args)
		{

			//AsyncContext.Run(() => MainAsync(args));
			
			foreach (int n in Program.GetEnumerator())
			{
				System.Console.Write(n + " ");
			}

		}
		static async void MainAsync(string[] args)
		{

			List<int> list = new List<int>() { 8,2,1,6,7};

			Dictionary<string, List<int>> bubleSortDict = await BubbleSort.BubleSortWithMethod(list, null);

			foreach (var item in bubleSortDict)
			{
				Write($@"Key={item.Key} Value= [");
				foreach (var value in item.Value)
				{
					Write($@" {value} ");
				}
				Write("] ");
			}
			/*
			 * Main program call method with delegate  in Selection Sort class
			 * Any preferred method send SelectionWithMethod as parameter 
			 * etc.DescendingSelectionSort Method or AscendingSelectionSort Method
			 */
			var selectionSortDict=SelectionSort.SelectionSortWithMethod(list, null,SelectionSort.AscendingSelectionsort);
			foreach (var item in selectionSortDict)
			{
				//Write(item);
			}

			QuickSort.QuickSortMergeWithMethod(list,null);

			/*
			 * we called insertion method because of extion method
		    */
			var insertionSort = list.InsertionSortMethod(null);
			foreach (var item in insertionSort)
			{
				Write(item);
			}

		
			Read();
		}
		public static IEnumerable<int> GetEnumerator()
		{
			for (int i = 0; i < 10; i++)
			{
				yield return i;
			}
			Read();
		}

	}
}
