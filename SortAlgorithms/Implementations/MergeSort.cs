﻿
using System.Collections.Generic;
using SortAlgorithms.Interfaces;

namespace SortAlgorithms.Implementations
{
	public class MergeSort : IMergeSort
	{
		/*
			 * Mergesort algorithms select middle element.OrderList  divide according to middle index
			 * Left of middleArray element recursive call than this array divide 2 array
			 * Right of middleArray element rescursive call tahna its divide 2 array
			 * While array element count 1,Recursive call is  continue.
			 * Consequently is achived 2 list called beforMidList and afterMidList
			 */
		public List<T> MergeSortRecursiveMethod<T>(List<T> ordereredList, Comparer<T> comparer, int leftIndex, int rightIndex)
		{
			comparer = comparer ?? Comparer<T>.Default;
			if (ordereredList.Count < 2)
			{
				return ordereredList;
			}
			if (ordereredList.Count == 2)
			{
				if (comparer.Compare(ordereredList[0], ordereredList[1]) > 0)
				{
					//var temp = ordereredList[0];
					//ordereredList[0] = ordereredList[1];
					//ordereredList[1] = temp;
					ordereredList.Swap(1,0);
				}
				return ordereredList;
			}
			else
			{
				int middleArrayIndex = (leftIndex + rightIndex) / 2;
				List<T> beforeMidList = ordereredList.GetRange(leftIndex, middleArrayIndex);
				List<T> afterMidList = ordereredList.GetRange(middleArrayIndex, (middleArrayIndex - leftIndex));
				if (ordereredList.Count % 2 != 0)
				{
					afterMidList = ordereredList.GetRange(middleArrayIndex, (middleArrayIndex - leftIndex) + 1);
				}


				beforeMidList = MergeSortRecursiveMethod<T>(beforeMidList, null, 0, beforeMidList.Count);
				afterMidList = MergeSortRecursiveMethod<T>(afterMidList, null, 0, afterMidList.Count);

				return MergeList(beforeMidList, afterMidList, comparer);
			}
		}
		/*
		 * MergeLİst method result of recursive call is produced beforeList and afterList
		 * These lists merge one List called result.
		 */
		public List<T> MergeList<T>(List<T> beforeList, List<T> afterList, Comparer<T> comparer)
		{
			List<T> result = new List<T>();
			result.AddRange(beforeList);
			result.AddRange(afterList);

			return result;
		}
	}
}
