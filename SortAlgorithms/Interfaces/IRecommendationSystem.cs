﻿

using System.Collections.Generic;
using SortAlgorithms.Implementations;

namespace SortAlgorithms.Interfaces
{
    interface IRecommendationSystem
    {
        double CalculatePearsonCorellation(string product1, string product2);
        void TopMatches(string product1);
        Dictionary<string, List<ReviewInformation>> AddProductReview();

    }
}
