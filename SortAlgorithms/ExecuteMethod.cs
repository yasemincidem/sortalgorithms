﻿using Nito.AsyncEx;
using System.Collections.Generic;
using SortAlgorithms.Implementations;
using static System.Console;

namespace SortAlgorithms
{
	public  class ExecuteMethod
	{
		public static  void Main(string[] args)
		{

			AsyncContext.Run(() => MainAsync(args));

		}
		static async void MainAsync(string[] args)
		{

			List<int> list = new List<int>() {4,1,7,2,8,0,5,15,14};

			Dictionary<string, List<int>> bubleSortDict = await BubbleSort.BubleSortWithMethod(list, null);

			foreach (var item in bubleSortDict)
			{
				Write($@"Key={item.Key} Value= [");
				foreach (var value in item.Value)
				{
					Write($@" {value} ");
				}
				Write("] ");
			}
			/*
			 * Main program call method with delegate  in Selection Sort class
			 * Any preferred method send SelectionWithMethod as parameter 
			 * etc.DescendingSelectionSort Method or AscendingSelectionSort Method
			 */
			var selectionSortDict=SelectionSort.SelectionSortWithMethod(list, null,SelectionSort.AscendingSelectionsort);
			foreach (var item in selectionSortDict)
			{
				//Write(item);
			}

			//QuickSort.QuickSortMergeWithMethod(list,null);

			/*
			 * we called insertion method because of extion method
		    */
			InsertionSort insertionSort=new InsertionSort();
			var insertSort = insertionSort.InsertionSortMethod(list, null);
			foreach (var item in insertSort)
			{
				//Write(item);
			}
			MergeSort mergeSort=new MergeSort();

			List<int> mergeList=mergeSort.MergeSortRecursiveMethod(list, null,0,list.Count);
			foreach (var item in mergeList)
			{
				Write(item);
			}
		    RecommendationSystem recommendation=new RecommendationSystem();
            recommendation.TopMatches("ACME Industrial Rocket Pack");
			Read();
		}
	

	}
}
