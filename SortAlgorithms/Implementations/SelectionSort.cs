﻿
using System.Collections.Generic;


namespace SortAlgorithms.Implementations
{
	//The delagate holds the reference of methods in selection sort class  
	//Because delegates allow methods to be passed as parameters	
	public delegate List<T> MergeMethodDelgate<T>(List<T> list, Comparer<T> comparer);

	public static class SelectionSort
	{
		//This method take delgate called with MergeMethodDelate hold that refernce of methods as parameter
		//Also this method extension method .So method is using in this way object.AscendingSelectionSort() 
		public static List<T> SelectionSortWithMethod<T>(List<T> orderedList, Comparer<T> comparer, MergeMethodDelgate<T> mergeMethod)
		{
			comparer = comparer ?? Comparer<T>.Default;
			return mergeMethod(orderedList, comparer);
		}
		/*
		 * SelectionSort algorithm first value hold min index.After search array other elements excepts of min index.
		 * if the other element of array is smaller than min value , the value replace min value and so array
		 * sort ascending according to selectionsort algorithm
		 * Also this method extension method .So method is using in this way object.AscendingSelectionSort() 
		 */
		public static List<T> AscendingSelectionsort<T>(this List<T> orderedList, Comparer<T> comparer)
		{
			List<T> newList = new List<T>();
			newList.AddRange(orderedList);

			for (int i = 0; i < newList.Count - 1; i++)
			{
				int min = i;
				for (int j = i + 1; j < newList.Count - 1; j++)
				{
					if (comparer.Compare(newList[j], newList[min]) < 0)
						min = j;
				}
				//var temp = newList[i];
				//newList[i] = newList[min];
				//newList[min] = temp;
				newList.Swap(min,i);
			}
			return newList;
		}
		 /*
		 * The last item of array hold min index.After index one by one is reduced and search in other elements of array
		 * if the other element of array is smaller than min value,the value replace with min value and so array
		 * sort descending according to selection sort algorithm.
		 */
		public static List<T> DescendingSelectionSort<T>(this List<T> orderedList, Comparer<T> comparer)
		{
			List<T> newList = new List<T>();
			newList.AddRange(orderedList);

			for (int i = newList.Count - 1; i > 0; i--)
			{
				int min = i;
				for (int j = i - 1; j > 0; j--)
				{
					if (comparer.Compare(newList[j], newList[min]) < 0)
						min = j;
				}
				//custom extension Swap method
				newList.Swap(min,i);
			}

			return newList;
		}
	}
}
