﻿using SortAlgorithms.Interfaces;
using System.Collections.Generic;


namespace SortAlgorithms.Implementations
{
	public class InsertionSort : IInsertionSort
	{
		/*
		 * Insertion Sort method this method select second of array and than previous value from this element compare second of array 
		 * if second element is samller than previous element,2 elemnts swapping 
		 */
		public List<T> InsertionSortMethod<T>(List<T> orderedList, Comparer<T> comparer)
		{
			comparer = comparer ?? Comparer<T>.Default;
			for (int i = 1; i <= orderedList.Count - 1; i++)
			{
				for (int j = i; j > 0; j--)
				{
					if (comparer.Compare(orderedList[j], orderedList[j - 1]) < 0)
					{
						//var temp = orderedList[j - 1];
						//orderedList[j - 1] = orderedList[j];
						//orderedList[j] = temp;
						orderedList.Swap(j,j-1);
					}
				}
			}

			return orderedList;

		}
	}
}
